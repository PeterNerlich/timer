v1.3
- add important information to about page
- add french translation thanks to Anne Onyme 017
- add spanish translation, thanks to Krakakanok
- increased button size

v1.2
- new maintainer danfro
- bumbed version to 1.2
- moved source code from launchpad to gitlab
- allow timers up to 24 hours

v1.1
- build for 16.04 framwork
- last version based on launchpad code

v1.0
- allow multiple timers
