// STORAGE FUNCTIONS FOR FAVOURITES TIMERS

.import QtQuick.LocalStorage 2.0 as Sql

function getDatabase(){
    return Sql.LocalStorage.openDatabaseSync("Timerpro_1", "1.0", "timerpro", 1000000);
}
/*============== CHECKING DB VERSION ===================*/

// Create special table for DB version number
function createDBVersionTable()
{
    var db = getDatabase();
    db.transaction(
                function(tx) {
                    tx.executeSql('CREATE TABLE IF NOT EXISTS dbversion' +
                                  '(dbversionvalue REAL)');
                }
                );
}

// Check if DB value exist
function checkIfDBVersionExist(db_version)
{
    var db = getDatabase();
    var result = ""
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT * FROM dbversion WHERE dbversionvalue = ?',
                                           [db_version]);
                    if (rs.rows.length > 0 ) {
                        result = "true";
                    }
                    else {
                        result = "false";
                    }
                }
                );
    return result
}

function checkIfDBVersionTableExists()
{
    var db = getDatabase();
    var result = ""
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT name FROM sqlite_master WHERE name="dbversion" AND type="table"');
                    if (rs.rows.length > 0 ) {
                        result = "exist";
                    }
                    else {
                        result = "not_exist";
                    }
                }
                );
    return result
}

// Save DB version to DB
function saveDBVersion(db_version)
{
    var db = getDatabase();
    var result = "";
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('INSERT INTO dbversion VALUES(?)',
                                           [db_version]);
                    if (rs.rowsAffected > 0) {
                        result = "DB version saved as: " + db_version;
                    } else {
                        result = "Error with saving DB version: " + db_version;
                    }
                }
                );
    console.log(result)
}
// Update DB version
function updateDBVersion(db_version)
{
    var db = getDatabase();
    var result = "";
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('UPDATE dbversion '+
                                           'SET dbversionvalue = ? ',
                                           [db_version]);
                    if (rs.rowsAffected > 0) {
                        result = "DB version saved as: " + db_version;
                    } else {
                        result = "Error with saving DB version: " + db_version;
                    }
                }
                );
    console.log(result)
}

// Check if favs table already exists
function checkIfFavsTableExists()
{
    var db = getDatabase();
    var result = ""
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT name FROM sqlite_master WHERE name="favs" AND type="table"');
                    if (rs.rows.length > 0 ) {
                        result = "table_exist";
                    }
                    else {
                        result = "table_not_exist";
                    }
                }
                );
    return result
}

// Add column to a table for alarm sounds
function addSoundsColumnToTable(){
    var db = getDatabase();
    db.transaction(
                function(tx){
                    tx.executeSql('ALTER TABLE favs ADD COLUMN fsound TEXT')
                }
                );
}

// Create favourites table
function createFavsTable(){
    var db = getDatabase();
    db.transaction(
                function(tx){
                    tx.executeSql('CREATE TABLE IF NOT EXISTS favs' +
                                  '(fname TEXT,' +
                                  'ftime INT,' +
                                  'tstamp TEXT,' +
                                  'fsound  TEXT)')
                }
                );
}

// Save favourites
function saveFavs(favname, favtime, favtimestamp, favtimesound){
    var db = getDatabase();
    var result = "";
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('INSERT INTO favs VALUES(?, ?, ?, ?)',
                                           [favname, favtime, favtimestamp, favtimesound]);
                }
                );
}

// Update favourites
function updateFavs(favname, favtime, favtimestamp, favtimesound){
    var db = getDatabase();
    var result = ""
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('UPDATE favs '+
                                           'SET fname = ?, ftime = ?, fsound = ? '+
                                           'WHERE tstamp = ?',
                                           [favname, favtime, favtimesound, favtimestamp]);
                }
                );
}

// Delete favourites
function deleteFavs(favtimestamp){
    var db = getDatabase();
    var result = ""
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('DELETE FROM favs WHERE tstamp = ?',[favtimestamp]);
                }
                );
}

// Get favourites from DB to ListView
function getFavs(favlistmodel)
{
    var db = getDatabase();
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT * FROM favs');
                    var fname = "";
                    var ftime = "";
                    var tstamp = "";
                    var fsound = "";
                    for(var i = 0; i < rs.rows.length; i++) {
                        fname = rs.rows.item(i).fname;
                        ftime = rs.rows.item(i).ftime;
                        tstamp = rs.rows.item(i).tstamp;
                        fsound = rs.rows.item(i).fsound;
                        favlistmodel.append({"name_of_fav": fname, "timertime": ftime, "favtimestamp": tstamp, "favtimesound": fsound});
                    }
                }
                );
}

// Create timers table
function createTimersTable(){
    var db = getDatabase();
    db.transaction(
                function(tx){
                    tx.executeSql('CREATE TABLE IF NOT EXISTS timers' +
                                  '(tname TEXT,' +
                                  'tendtime INT,' +
                                  'tduration INT,' +
                                  'tsound TEXT,' +
                                  'trunning INT,' +
                                  'tstamp  TEXT)')
                }
                );
}

// Save timer
function saveTimers(tname, tendtime, tduration, tsound, trunning, tstamp){
    var db = getDatabase();
    var result = "";
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('INSERT INTO timers VALUES(?, ?, ?, ?, ?, ?)',
                                           [tname, tendtime, tduration, tsound, trunning, tstamp]);
                }
                );
}

// Update timer
function updateTimers(tname, tendtime, tduration, tsound, trunning, tstamp){
    var db = getDatabase();
    var result = ""
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('UPDATE timers '+
                                           'SET tname = ?, tendtime = ?, tduration = ?, tsound = ?, trunning = ? '+
                                           'WHERE tstamp = ?',
                                           [tname, tendtime, tduration, tsound, trunning, tstamp]);
                }
                );
}

// Delete timer
function deleteTimers(tstamp){
    var db = getDatabase();
    var result = ""
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('DELETE FROM timers WHERE tstamp = ?',[tstamp]);
                }
                );
}

// Get timers from DB
function getTimers(timerslistmodel)
{
    var db = getDatabase();
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT * FROM timers');
                    var tname = "";
                    var tendtime = "";
                    var tduration = "";
                    var tsound = "";
                    var trunning = "";
                    var tstamp = "";

                    for(var i = 0; i < rs.rows.length; i++) {
                        tname = rs.rows.item(i).tname;
                        tendtime = rs.rows.item(i).tendtime;
                        tduration = rs.rows.item(i).tduration;
                        tsound = rs.rows.item(i).tsound;
                        trunning = rs.rows.item(i).trunning == 1;
                        tstamp = rs.rows.item(i).tstamp;
                        timerslistmodel.append({"nameOfTimer": tname, "endTime": tendtime, "durationOfTimer": tduration, "soundOfTimer": tsound, "isItRunning": trunning, "timerStamp": tstamp});
                    }
                }
                );
}

// Get last timer from DB
function getTimer(setter)
{
    var db = getDatabase();
    db.transaction(
                function(tx) {
                    var rs = tx.executeSql('SELECT * FROM timers');
                    //                    var tname = "";
                    //                    var tendtime = "";
                    //                    var tduration = "";
                    //                    var tsound = "";
                    //                    var trunning = "";
                    //                    var tstamp = "";


                    var  tname = rs.rows.item(0).tname;
                    var  tendtime = rs.rows.item(0).tendtime;
                    var  tduration = rs.rows.item(0).tduration;
                    var  tsound = rs.rows.item(0).tsound;
                    var  trunning = rs.rows.item(0).trunning == 1;
                    var  tstamp = rs.rows.item(0).tstamp;
                    setter.isRunning = trunning
                    setter.timername = tname
                    setter.timersound = tsound
                    setter.timerduration = tduration
                    setter.tempendtime = tendtime
//                    setter.stamp = tstamp
//                    timerslistmodel.append({"nameOfTimer": tname, "endTime": tendtime, "durationOfTimer": tduration, "soundOfTimer": tsound, "isItRunning": trunning, "timerStamp": tstamp});

                }
                );
}
