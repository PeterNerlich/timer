TEMPLATE = aux
TARGET = Timer

RESOURCES += Timer.qrc

QML_FILES += $$files(*.qml,true) \
             $$files(*.js,true)

CONF_FILES +=  Timer.apparmor \
               Timer.png


OTHER_FILES += $${CONF_FILES} \
               $${QML_FILES} \
               Timer.desktop

#specify where the qml/js files are installed to
qml_files.path = /Timer
qml_files.files += $${QML_FILES}

#specify where the config files are installed to
config_files.path = /Timer
config_files.files += $${CONF_FILES}

#install the desktop file, a translated version is
#automatically created in the build directory
desktop_file.path = /Timer
desktop_file.files = $$OUT_PWD/Timer.desktop
desktop_file.CONFIG += no_check_exist

#images files
my_images.path = /Timer
my_images.files += images/*

#sound files
my_sounds.path = /Timer
my_sounds.files += sounds/*

INSTALLS+=config_files qml_files desktop_file my_images my_sounds

DISTFILES += \
    Warning.qml \
    SettingsPage.qml \
    DefaultSoundPage.qml \
    ThemePage.qml \
    AboutPage.qml \
    ClickyIcon.qml \
    EditTimer.qml \
    FavPage.qml \
    ProgressCircle.qml \
    TimerListItem.qml \
    TimeSetter.qml \
    Storage.js \
    MainPage.qml \
    TimerSetterMain.qml \
    TimerElement.qml \
    TimerPreviewPage.qml \
    TimerSetterExtra.qml \
    SoundSettingExtra.qml \
    InfoBubble.qml \
    InfoPage.qml

