import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: root_themepage

    property string temp_theme

    header: PageHeader {
        id: main_header

        title: i18n.tr("Timer theme")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color}
        trailingActionBar.actions: Action {
            text: "confirm"
            iconName: "ok"
            onTriggered: {
                current_theme = temp_theme
                apl_main.removePages(root_themepage)
            }
        }
    }

    ListView {
        id: themes_listview

        width: parent.width
        height: parent.height - main_header.height
        anchors.top: main_header.bottom
        clip: true

        model: ["Colores", "Imitatio", "Standard"]

        delegate:
            ListItem {
            id: theme_item

            height: main_layout.height + divider.height
            divider.anchors.leftMargin: units.gu(6)

            ListItemLayout {
                id: main_layout

                title.text: modelData

                Icon {
                    name: "tick"
                    color: main_layout.title.color
                    width: units.gu(2)
                    height: width
                    SlotsLayout.position: SlotsLayout.Leading
                    opacity: modelData == temp_theme ? 1 : 0
                }
                Image {
                    source: Qt.resolvedUrl(/*"images/" + */modelData + "/preview.png")
                    width: units.gu(6)
                    height: width
                }
            }
            onClicked: {
                temp_theme = modelData
            }
        }
    }
}
