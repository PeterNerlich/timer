import QtQuick 2.4

// Based on http://stackoverflow.com/a/22903361

// draws two arcs (portion of a circle)
Canvas {
    id: canvas

    width: parent.width
    height: parent.height
    antialiasing: true

    property color backColor: progress_back_color
    property color mainColor: progress_color

    property real centerWidth: width / 2
    property real centerHeight: height / 2
    property real radius: width / 2 > units.dp(2) ? width / 2 - units.dp(2) : 1

    property real minimumValue: 0
    property real maximumValue: 100
    property real currentValue

    // this is the angle that splits the circle in two arcs
    // first arc is drawn from 0 radians to angle radians
    // second arc is angle radians to 2*PI radians
    property real angle: (currentValue - minimumValue) / (maximumValue - minimumValue) * 2 * Math.PI

    // we want both circle to start / end at 12 o'clock
    // without this offset we would start / end at 9 o'clock
    property real angleOffset: - Math.PI / 2

    onBackColorChanged: requestPaint()
    onMainColorChanged: requestPaint()
    onMinimumValueChanged: requestPaint()
    onMaximumValueChanged: requestPaint()
    onCurrentValueChanged: requestPaint()

    onPaint: {
        var ctx = getContext("2d");
        ctx.save();

        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // From angle to 2*PI
        ctx.beginPath();
        ctx.lineWidth = units.dp(2);
        ctx.strokeStyle = backColor;
        ctx.arc(canvas.centerWidth,
                canvas.centerHeight,
                canvas.radius,
                angleOffset + canvas.angle,
                angleOffset + 2 * Math.PI);
        ctx.stroke();

        // From 0 to angle
        ctx.beginPath();
        ctx.lineWidth = units.dp(2);
        ctx.strokeStyle = canvas.mainColor;
        ctx.arc(canvas.centerWidth,
                canvas.centerHeight,
                canvas.radius,
                canvas.angleOffset,
                canvas.angleOffset + canvas.angle);
        ctx.stroke();
        ctx.restore();
    }

    Behavior on currentValue {NumberAnimation{duration: 165}}

//    Connections {
//        target: time_text
//        onTextChanged: canvas.currentValue = new_time_setter.timelength / tempduration * canvas.maximumValue
//    }
}

