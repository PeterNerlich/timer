import QtQuick 2.4
import Ubuntu.Components 1.3
import "Storage.js" as Storage

Page {
    id: main_page

    property alias main_time_setter: main_time_setter
    property alias timers_model: timers_model
    property bool isMainPageLandscape: main_page.width > main_page.height

    Component.onCompleted: {
        Storage.getTimers(timers_model)
    }

    header: main_header

    PageHeader {
        id: main_header

        visible: main_page.header === main_header
        title: timers_model.count > 1 ? i18n.tr("Timers")
                                      : i18n.tr("Timer")
        StyleHints {backgroundColor: "transparent"; foregroundColor: top_text_color; dividerColor: "transparent"}

        trailingActionBar.actions: [

            Action {
                iconName: "add"
                text: i18n.tr("Add new timer")
                visible: timers_model.count > 0 && timers_model.count < 4
                onTriggered: {
                    new_timer_ldr.source = Qt.resolvedUrl("TimerSetterExtra.qml")
                    main_page.header = add_timer_header
                    infoVisible = false
                }
            },
            Action {
                iconName: "starred"
                text: i18n.tr("Favourites")

                onTriggered: {
                    isOneColumnLayout = false
                    apl_main.addPageToNextColumn(apl_main.primaryPage, fav_page)
                    infoVisible = false
                }
            },
            Action {
                iconName: isNightMode ? "display-brightness-max" : "night-mode"
                text: isNightMode ? i18n.tr("Day Mode") : i18n.tr("Night Mode")

                onTriggered: {
                    infoVisible = false
                    isNightMode = !isNightMode
                }
            },
            Action {
                iconName: "settings"
                text: i18n.tr("Settings")

                onTriggered: {
                    isOneColumnLayout = false
                    apl_main.addPageToNextColumn(apl_main.primaryPage, Qt.resolvedUrl("SettingsPage.qml"))
                    infoVisible = false
                }
            },
            Action {
                iconName: "info"
                text: i18n.tr("About")

                onTriggered: {
                    isOneColumnLayout = false
                    apl_main.addPageToNextColumn(apl_main.primaryPage, Qt.resolvedUrl("AboutPage.qml"))
                    infoVisible = false
                }
            }
        ]
    }

    PageHeader {
        id: add_timer_header

        visible: main_page.header === add_timer_header
        title: i18n.tr("Add new timer")
        StyleHints {backgroundColor: "transparent"; foregroundColor: top_text_color; dividerColor: "transparent"}
        leadingActionBar.actions: [
            Action {
                iconName: "close"

                onTriggered: {
                    new_timer_ldr.source = ""
                    main_page.header = main_header
                }
            }

        ]
    }

    PageHeader {
        id: timer_preview_header

        visible: main_page.header === timer_preview_header
        title: i18n.tr("Timer details")
        StyleHints {backgroundColor: "transparent"; foregroundColor: top_text_color; dividerColor: "transparent"}
        leadingActionBar.actions: [
            Action {
                iconName: "back"

                onTriggered: {
                    timer_preview_ldr.item.setTimer()
                    timer_preview_ldr.source = ""
                    main_page.header = main_header
                }
            }

        ]
    }

    TimerSetterMain {
        id: main_time_setter

        visible: timers_model.count <= 1
        anchors {
            top: parent.top
            topMargin: units.gu(6)
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Connections {
            target: timers_model

            onCountChanged: {
                if(timers_model.count === 1){
                    main_time_setter.isRunning = timers_model.get(0).isItRunning
                    main_time_setter.timerduration = timers_model.get(0).durationOfTimer
                    main_time_setter.tempendtime = timers_model.get(0).endTime
                    main_time_setter.timername = timers_model.get(0).nameOfTimer
                    main_time_setter.timersound = timers_model.get(0).soundOfTimer
                    if (!timers_model.get(0).isItRunning) {
                        timeLenghtToHMS(main_time_setter.timerduration, main_time_setter.time_setter)
                        main_time_setter.timerFinished = false
                    }
                }
            }
        }
    }

    ListModel {
        id: timers_model
    }

    GridView {
        id: timers_grid

        visible: count > 1
        anchors {
            fill: parent
            topMargin: units.gu(6)
        }
        cellWidth: isMainPageLandscape ? main_page.width / 2
                                       : main_page.width
        cellHeight: isMainPageLandscape ? (main_page.height - units.gu(6)) / ((count + count % 2) / 2)
                                        : (main_page.height - units.gu(6)) / count
        interactive: false
        delegate: TimerElement {
            width: timers_grid.cellWidth
            height: timers_grid.cellHeight
        }
        model: timers_model
    }

    Loader {
        id: timer_preview_ldr

        anchors {
            fill: parent
            topMargin: units.gu(6)

        }
    }

    Loader {
        id: new_timer_ldr

        anchors {
            fill: parent
            topMargin: units.gu(6)
        }
    }

    InfoBubble {
        id: info_new_timer

        visible: infoVisible && timers_model.count > 0
        anchors {
            fill: parent
            topMargin: units.gu(6)
        }
    }


}

