import QtQuick 2.4
import Ubuntu.Components 1.3
import QtMultimedia 5.6


Rectangle {
    id: root_defaultsoundpage

    property alias listview: settings_listview
    property alias audio_play: audio_play
    property string temp_sound

    color: main_back_color

     PageHeader {
        id: main_header

        title: i18n.tr("Timer sound")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color}
        leadingActionBar.actions: Action {
            text: "back"
            iconName: "back"
            onTriggered: {
                audio_play.stop()
                sound_ldr.source = ""
            }
        }
        trailingActionBar.actions: Action {
            text: "confirm"
            iconName: "ok"
            onTriggered: {
                audio_play.stop()
                timersound = temp_sound
                sound_ldr.source = ""
            }
        }
    }

    Audio {
        id: audio_play

        source: Qt.resolvedUrl(getSoundFile(temp_sound))
        loops: Audio.Infinite
        audioRole: Audio.AlarmRole
    }

    ListItem {
        id: stop_play_item

        width: parent.width
        height: stop_play_button.height + units.gu(2)
        anchors.top: main_header.bottom

        Button {
            id: stop_play_button

            property bool isPlaying: audio_play.playbackState == Audio.PlayingState

            width: Math.min(parent.width - units.gu(4), units.gu(22))
            anchors.centerIn: parent
            text: isPlaying ? i18n.tr("Stop playing") : i18n.tr("Play selected")
            onClicked: isPlaying ? audio_play.stop() : audio_play.play()
        }
    }

    ListView {
        id: settings_listview

        width: parent.width
        height: parent.height - main_header.height - stop_play_item.height
        anchors.top: stop_play_item.bottom
        clip: true

        model: [
            "sounds/Beeps.ogg",
            "sounds/Buzz.ogg",
            "sounds/Campanula.ogg",
            "sounds/Drrr.ogg",
            "sounds/Fanfare.ogg",
            "sounds/Harp.ogg",
            "sounds/Joy.ogg",
            "sounds/Military.ogg",
            "sounds/Progressive.ogg",
            "sounds/Riff.ogg",
            "sounds/Tempus.ogg",
            "sounds/Woods.ogg"
        ]

        delegate:
            ListItem {
            id: sound_item

            height: main_layout.height + divider.height
            divider.anchors.leftMargin: units.gu(6)

            ListItemLayout {
                id: main_layout
                title.text: getFileName(modelData)

                Icon {
                    name: "tick"
                    color: main_layout.title.color
                    width: units.gu(2)
                    height: width
                    SlotsLayout.position: SlotsLayout.Leading
                    opacity: modelData == temp_sound ? 1 : 0
                }
            }
            onClicked: {
                temp_sound = modelData
            }
        }
    }
}

