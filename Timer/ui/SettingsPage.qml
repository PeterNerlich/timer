import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: root_settingpage

    header: PageHeader {
        id: main_header

        title: i18n.tr("Settings")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color}
                leadingActionBar.actions: [
                    Action {
                        visible: isWide
                        text: "close"
                        iconName: "close"
                        onTriggered: {
                            isOneColumnLayout = true
                            apl_main.removePages(root_settingpage)
                        }
                    },
                    Action {
                        visible: !isWide
                        text: "close"
                        iconName: "back"
                        onTriggered: {
                            isOneColumnLayout = true
                            apl_main.removePages(root_settingpage)
                        }
                    }
                ]
    }

    Column {
        id: settings_column

        anchors {
            top: main_header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        ListItem {
            id: display_on_setting_item
            height: l_displ.height + divider.height

            ListItemLayout {
                id: l_displ
                title.text: i18n.tr("Keep display on")

                CheckBox {
                    id: display_checkbox
                    checked: isDisplayOn
                    onCheckedChanged: isDisplayOn = checked
                    SlotsLayout.position: SlotsLayout.Leading
                }
            }
            onClicked: display_checkbox.checked = !display_checkbox.checked
        }

        ListItem {
            id: sound_setting_item
            height: l_sound.height + divider.height

            ListItemLayout {
                id: l_sound
                title.text: i18n.tr("Default sound")

                Label { text: getFileName(defaultsound) }
                ProgressionSlot{}
            }
            onClicked: {
                apl_main.addPageToCurrentColumn(root_settingpage, Qt.resolvedUrl("DefaultSoundPage.qml"), {isDefaultSoundEdited: true, temp_sound: defaultsound})
            }
        }

        ListItem {
            id: theme_setting_item

            divider.visible: false

            ListItemLayout {
                title.text: i18n.tr("Theme")

                Label { text: current_theme }
                ProgressionSlot{}
            }
            onClicked: apl_main.addPageToCurrentColumn(root_settingpage, Qt.resolvedUrl("ThemePage.qml"), {temp_theme: current_theme})
        }
    }
}
