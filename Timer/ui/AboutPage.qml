import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: root_about

    header: PageHeader {
        id: main_header

        title: i18n.tr("About")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color}
        leadingActionBar.actions: [
            Action {
                visible: isWide
                text: "close"
                iconName: "close"
                onTriggered: {
                    isOneColumnLayout = true
                    apl_main.removePages(root_about)
                }
            },
            Action {
                id: collapse_favs
                visible: !isWide
                text: "close"
                iconName: "back"
                onTriggered: {
                    isOneColumnLayout = true
                    apl_main.removePages(root_about)
                }
            }
        ]
        extension: Sections {
            id: header_sections
            StyleHints {selectedSectionColor: top_text_color; sectionColor: isNightMode ? "#888" : "#999"}
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            model: [i18n.tr("General"), i18n.tr("Credits"), i18n.tr("Important")]
        }
    }

    Flickable {
        id: page_flickable

        anchors {
            top: main_header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight:  main_column.height + units.gu(2)
        clip: true

        Column {
            id: main_column

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                leftMargin: units.gu(1)
                rightMargin: units.gu(1)
            }
            
            Item {
                id: icon

                visible: header_sections.selectedIndex === 0
                width: parent.width
                height: app_icon.height + units.gu(4)

                UbuntuShape {
                    id: app_icon

                    width: Math.min(root_about.width/3, 256)
                    height: width
                    anchors.centerIn: parent

                    source: Image {
                        id: icon_image
                        source: Qt.resolvedUrl("Timer.png")
                    }
                    radius: "small"
                    aspect: UbuntuShape.DropShadow
                }
            }

            Label {
                id: name

                visible: header_sections.selectedIndex === 0
                text: i18n.tr("Timer") + "\nv1.3"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: units.gu(4)
                textSize: Label.Large
                horizontalAlignment:  Text.AlignHCenter
            }

            Label {
                id: description_text
                
                visible: header_sections.selectedIndex === 0
                width: parent.width
                text: i18n.tr("up to 4 individual timers") +"\n"
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.WordWrap
                horizontalAlignment:  Text.AlignHCenter
            }

            ListItem {
                visible: header_sections.selectedIndex === 0
                height: l_bugs.height + divider.height
                ListItemLayout {
                    id: l_bugs
                    title.text: i18n.tr("Report bugs on GitLab")

                    ProgressionSlot{}
                }
                onClicked: Qt.openUrlExternally('https://gitlab.com/Danfro/timer/issues')
            }

            ListItem {
                visible: header_sections.selectedIndex === 0
                height: l_trans.height + divider.height
                ListItemLayout {
                    id: l_trans
                    title.text: i18n.tr("Help with translation via GitLab")

                    ProgressionSlot{}
                }
                onClicked: Qt.openUrlExternally('https://gitlab.com/Danfro/timer/tree/master/po')
            }
            
            ListItem {
                visible: header_sections.selectedIndex === 0
                height: l_apps.height + divider.height
                ListItemLayout {
                    id: l_apps
                    title.text: i18n.tr("Timer app in OpenStore")

                    ProgressionSlot{}
                }
                onClicked: Qt.openUrlExternally('https://open-store.io/app/timerpro.mivoligo')
            }
            
            ListItem {
                visible: header_sections.selectedIndex === 0
                height: l_license.height + divider.height
                ListItemLayout {
                    id: l_license
                    title.text: i18n.tr("License") + ": BSD-3-Clause"
                    
                    ProgressionSlot{}
                }
                onClicked: {Qt.openUrlExternally('https://opensource.org/licenses/BSD-3-Clause')}
            }
            
            ListItem {
                visible: header_sections.selectedIndex === 0
                divider.visible: false
                ListItemLayout {
                    title.text: i18n.tr("Donate to ubports")

                    ProgressionSlot {
                        name: "like"
                        color: UbuntuColors.red
                        height: units.gu(3)
                        width: height
                    }
                }
                
                //onClicked: Qt.openUrlExternally('https://www.paypal.me/miv')
                //Donate link changed to UBports donate page
                onClicked: Qt.openUrlExternally('https://ubports.com/de_DE/donate')
            }

            
            Label {
                id: actualdev

                visible: header_sections.selectedIndex === 1
                text: "\n" + i18n.tr("App development since version v1.2")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }
            
            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_maintainer.height + divider.height
                ListItemLayout {
                    id: l_maintainer
                    title.text: i18n.tr("Maintainer") + ": Daniel Frost"
                    ProgressionSlot{}
                }
                onClicked: {Qt.openUrlExternally('https://gitlab.com/Danfro')}
            }
            
            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_french.height - units.gu(1)
                divider.visible: false
                ListItemLayout {
                    id: l_french
                    title.text: i18n.tr("French") + ": Anne Onyme 017"
                    ProgressionSlot{}
                }
                onClicked: {Qt.openUrlExternally('https://gitlab.com/Anne17')}
            }
            
            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_spanish.height - units.gu(1)
                divider.visible: false
                ListItemLayout {
                    id: l_spanish
                    title.text: i18n.tr("Spanish") + ": Krakakanok"
                    ProgressionSlot{}
                }
                onClicked: {Qt.openUrlExternally('https://gitlab.com/Krakakanok')}
            }
            
            Label {
                id: historicaldev

                visible: header_sections.selectedIndex === 1
                text: "\n" + i18n.tr("App development up to version v1.1")
                anchors.horizontalCenter: parent.horizontalCenter
                font.bold : true
                horizontalAlignment:  Text.AlignHCenter
            }
            
            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_author.height
                ListItemLayout {
                    id: l_author
                    title.text: i18n.tr("Author") + ": Michał Prędotka"
                    ProgressionSlot{}
                }
                onClicked: {Qt.openUrlExternally('http://mivoligo.com')}
            }
                        
            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_sound.height + divider.height
                ListItemLayout {
                    id: l_sound
                    title.text: i18n.tr("Sounds") + ": Tyrel Parker"
                    ProgressionSlot{}
                }
                onClicked: {Qt.openUrlExternally('https://plus.google.com/u/0/108131512413210328857/about')}
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_icon.height + divider.height
                ListItemLayout {
                    id: l_icon
                    title.text: i18n.tr("Icon design") + ": Sam Hewitt"
                    ProgressionSlot{}
                }
                onClicked: {Qt.openUrlExternally('https://samuelhewitt.com﻿')}
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                height: l_test.height + divider.height
                ListItemLayout {
                    id: l_test
                    title.text: i18n.tr("Testing") + ": Sergi Quiles Pérez"
                    ProgressionSlot{}
                }
                onClicked: {Qt.openUrlExternally('https://plus.google.com/u/0/+SergiQuilesPérez')}
            }

            ListItem {
                visible: header_sections.selectedIndex === 1
                divider.visible: false
                ListItemLayout {
                    title.text: i18n.tr("Special thanks") + ": nik90"
                    //ProgressionSlot{}
                }
                //Link does not work anymore -> disabled
                //onClicked: {Qt.openUrlExternally('http://nik90.com')}
            }
            
            //Warning Label, changes need to go to: /Timer/ui/Warning.qml, README.md and OpenStore description text
            Label {
                id: warning
                
                visible: header_sections.selectedIndex === 2
                
                horizontalAlignment: Text.AlignJustify //Text.AlignHCenter
                width: parent.width //- units.gu(2)
                text: "\n" 
                    + i18n.tr("Please notice that the app depends on the Alarm API which has some limitations. There are a few things you need to know:")
                    + "\n\n"
                    + "1. " + i18n.tr("Setting timers shorter than 1 minute is not recommended. Due to system limitations, the notifications about such timers are not displayed at the right time. Sorry.")
                    + "\n\n"
                    + "2. " + i18n.tr("Sound volume is the same as set in the Clock app for alarms.")
                    + "\n\n"
                    + "3. " + i18n.tr("Changing time settings or time zone when timer is running will confuse the timer.")
                    + "\n\n"
                    + "4. " + i18n.tr("Timers are registered as alarms within the clock app. Once the timer has elapsed it is removed from clock app.")
                    + "\n\n"
                    +i18n.tr("Feel free to report other problems.")
                wrapMode: Text.WordWrap
            }
        }
    }
}
