import QtQuick 2.4
import Ubuntu.Components 1.3
import "Storage.js" as Storage

Rectangle {
    id: root_timesettermain

    property bool isRunning
    property string timername
    property var timersound
    property var tempendtime
    property bool isZero: time_text.text == "00:00:00"
    property bool showsTime: false
    property var timerduration
    property alias time_setter: new_time_setter
    property real restHeight: time_text.implicitHeight + timer_end_time.implicitHeight + timer_name_field.implicitHeight + start_button.implicitHeight + edit_sound.implicitHeight + units.gu(5)
    property real circleWidth: (root_timesettermain.width > root_timesettermain.height) && !isHigh ? Math.min( root_timesettermain.width / 2, root_timesettermain.height - units.gu(4), units.gu(60) )
                                                                                                   : Math.min( root_timesettermain.width - units.gu(4), root_timesettermain.height - restHeight, ( root_timesettermain.height / 2 - (time_text.implicitHeight + timer_end_time.implicitHeight + timer_name_lbl.implicitHeight + units.gu(1.5)) ) * 2,  units.gu(60))


    function setTimer()
    {
        if(!isRunning) {
            timerduration = new_time_setter.h * 60 * 60 * 1000 + new_time_setter.m * 60 * 1000 + new_time_setter.s * 1000
            tempendtime = tempEndTime(new_time_setter.h, new_time_setter.m, new_time_setter.s)
            timername = timer_name_field.text
            timers_model.set(timers_grid.currentIndex, {
                                 nameOfTimer: timername,
                                 soundOfTimer: timersound,
                                 durationOfTimer: timerduration
                             }
                             )
            timers_grid.currentItem.timerDuration = timerduration
        }
    }

    function startTimer()
    {
        setTimer()
        timers_grid.currentItem.startTimer()
        isRunning = true
    }

    function stopTimer()
    {
        isRunning = false
        showsTime = false
        timers_grid.currentItem.stopTimer()
    }

    function resetTimer()
    {
        new_time_setter.h = 0
        new_time_setter.m = 0
        new_time_setter.s = 0
        new_time_setter.timelength = 0
    }

    function remainingTime()
    {
        var start = Date.now()
        var end = new Date(tempendtime)
        var remain = end - start

        if (remain >= 0){
            return [new_time_setter.timelength = remain,

                    timeLenghtToHMS(remain, new_time_setter)
                    ]
        }

        else {
            return [sinceFinish = timeLenghtToDate(Math.abs(remain)),
                    timer_finished.visible = true,

                    showsTime = false
                    ]
        }
    }

    function endTime()
    {
        var endTimeInMs = tempEndTime(new_time_setter.h, new_time_setter.m, new_time_setter.s)
        var endTimeV = Qt.formatTime(new Date(endTimeInMs), "hh:mm:ss")
        var today = Qt.formatDateTime(new Date(), "d MMM yyyy")
        var endDay = Qt.formatDateTime(new Date(endTimeInMs), "d MMM yyyy")

        if (today === endDay) {
            return endTimeV
        }
        else {
            return i18n.tr("tomorrow") + " " + endTimeV
        }
    }

    color: main_back_color

    Timer {
        id: refreshTimer

        interval: 100
        repeat: true
        running: isRunning

        onTriggered: {
            remainingTime()
        }
    }

    LiveTimer {
        id: checkEndTime

        frequency: isZero ? LiveTimer.Disabled : LiveTimer.Second
        // TRANSLATORS: It's about the time at which a timer will finish,
        // It will read for example "Finish 12:45"
        onTrigger: timer_end_time.text = i18n.tr("Finish") + " " + endTime()
    }

    // DELETE TIMER
    ClickyIcon {
        id: delete_timer_btn

        visible: isZero
        iconcolor: UbuntuColors.red
        iconname: "delete"
        anchors {
            right: time_text.left
            rightMargin: units.gu(1)
            verticalCenter: time_text.verticalCenter
        }

        onClicked: {
            timers_grid.currentItem.removeTimer() // delete timer
            timer_preview_ldr.source = ""
            main_page.header = main_header
        }
    }

    // RESET TIMER TO ZERO
    ClickyIcon {
        id: reset_button

        visible: !isZero && !isRunning
        iconcolor: UbuntuColors.red
        iconname: "reset"
        anchors {
            right: time_text.left
            rightMargin: units.gu(1)
            verticalCenter: time_text.verticalCenter
        }

        onClicked: resetTimer()

    }


    // TIMER TEXT CLOCK

    Button {
        id: hour_btn

        color: "transparent"
        width: time_text.height
        height: width
        anchors {
            left: time_text.left
            verticalCenter: time_text.verticalCenter
        }

        Keys.onPressed: {
            var key = event.key
            if(keys01.indexOf(key) > -1) {
                if(time_setter.h === 0 || time_setter.h >= 2) {
                    time_setter.h = keys01.indexOf(key)
                }
                else {
                    time_setter.h = 10 * time_setter.h + keys01.indexOf(key)
                }
            }
            else if(keys29.indexOf(key) > -1) {
                time_setter.h = keys29.indexOf(key) + 2
            }
            else if(key === Qt.Key_Up) {
                if (time_setter.h < 11) {
                    time_setter.h += 1
                }
            }
            else if(key === Qt.Key_Down) {
                if (time_setter.h > 0) {
                    time_setter.h -= 1
                }
            }
            else if(keyERS.indexOf(key) > -1) {
                if (!isZero) {
                    startTimer()
                    main_page.header = main_header
                }
            }
        }
    }

    Button {
        id: min_btn

        color: "transparent"
        width: time_text.height
        height: width
        anchors.centerIn: time_text

        Keys.onPressed: {
            var key = event.key
            if(keys05.indexOf(key) > -1) {
                if(time_setter.m === 0 || time_setter.m >= 6) {
                    time_setter.m = keys05.indexOf(key)
                }
                else {
                    time_setter.m = 10 * time_setter.m + keys05.indexOf(key)
                }
            }
            else if(keys69.indexOf(key) > -1) {
                if(time_setter.m === 0 || time_setter.m >= 6) {
                    time_setter.m = keys69.indexOf(key) + 6
                }
                else {
                    time_setter.m = 10 * time_setter.m + (keys69.indexOf(key) + 6)
                }
            }
            else if(key === Qt.Key_Up) {
                if (time_setter.m < 59) {
                    time_setter.m += 1
                }
            }
            else if(key === Qt.Key_Down) {
                if (time_setter.m > 0) {
                    time_setter.m -= 1
                }
            }
            else if(keyERS.indexOf(key) > -1) {
                if (!isZero) {
                    startTimer()
                    main_page.header = main_header
                }
            }
        }
    }

    Button {
        id: sec_btn

        color: "transparent"
        width: time_text.height
        height: width
        anchors {
            right: time_text.right
            verticalCenter: time_text.verticalCenter
        }

        Keys.onPressed: {
            var key = event.key
            if(keys05.indexOf(key) > -1) {
                if(time_setter.s === 0 || time_setter.s >= 6) {
                    time_setter.s = keys05.indexOf(key)
                }
                else {
                    time_setter.s = 10 * time_setter.s + keys05.indexOf(key)
                }
            }
            else if(keys69.indexOf(key) > -1) {
                if(time_setter.s === 0 || time_setter.s >= 6) {
                    time_setter.s = keys69.indexOf(key) + 6
                }
                else {
                    time_setter.s = 10 * time_setter.s + (keys69.indexOf(key) + 6)
                }
            }
            else if(key === Qt.Key_Up) {
                if (time_setter.s < 59) {
                    time_setter.s += 1
                }
            }
            else if(key === Qt.Key_Down) {
                if (time_setter.s > 0) {
                    time_setter.s -= 1
                }
            }
            else if(keyERS.indexOf(key) > -1) {
                if (!isZero) {
                    startTimer()
                    main_page.header = main_header
                }
            }
        }
    }

    // ADD TO FAVS
    ClickyIcon {
        id: add_to_fav_button

        visible: !isZero && !isRunning
        iconcolor: digit_clock_color
        iconsource: Qt.resolvedUrl("add-to-favs.svg")
        anchors {
            left: time_text.right
            leftMargin: units.gu(1)
            verticalCenter: time_text.verticalCenter
        }
        onClicked: {
            // load saving page
            isOneColumnLayout = false
            apl_main.addPageToNextColumn(apl_main.primaryPage, edit_timer_page, { isEdit: false, isFromMain: true })
            edit_timer_page.time_setter.h = new_time_setter.h
            edit_timer_page.time_setter.m = new_time_setter.m
            edit_timer_page.time_setter.s = new_time_setter.s
            edit_timer_page.timer_name_field.forceActiveFocus()
            edit_timer_page.timer_name_field.text = timername
            alarmsound = timersound
        }
    }

    // Timer time
    Text {
        id: time_text

        visible: !showsTime || !smallcentercircle.visible
        color: isRunning ? digit_clock_color : UbuntuColors.blue
        anchors {
            top: parent.top
            horizontalCenter: parent.horizontalCenter
        }
        text: unitsdisplay(new_time_setter.h) + ":" + unitsdisplay(new_time_setter.m) + ":" + unitsdisplay(new_time_setter.s)
        font.pixelSize: Math.min( parent.width / 6, units.gu(10) )
        Behavior on font.pixelSize{UbuntuNumberAnimation{}}
    }

    // TIMER END TIME
    Label {
        id: timer_end_time

        visible: !isZero
        width: parent.width - units.gu(4)
        anchors {
            top: time_text.bottom
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment:  Text.AlignHCenter
    }

    // TIMER NAME
    Label {
        id: timer_name_lbl

        visible: isRunning
        width: Math.min(parent.width - units.gu(4), units.gu(56))
        anchors {
            top: timer_end_time.bottom
            topMargin: units.gu(.5)
            horizontalCenter: parent.horizontalCenter
        }
        text: timername
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }


    TextField {
        id: timer_name_field

        visible: !isRunning
        width: Math.min(parent.width - units.gu(4), units.gu(56))
        anchors {
            top: timer_end_time.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
        }
        text: timername
        placeholderText: i18n.tr("Timer name")
        inputMethodHints: Qt.ImhNoPredictiveText
    }

    // TIMER SOUND

    Row {
        id: sound_row

        visible: isRunning
        spacing: units.gu(1)
        anchors {
            top: timer_name_lbl.bottom
            topMargin: new_time_setter.visible ? 2 * (parent.height / 2 - (time_text.implicitHeight + timer_end_time.implicitHeight + timer_name_lbl.implicitHeight + units.gu(3.5)))
                                               : units.gu(.5)
            horizontalCenter: parent.horizontalCenter
        }
        Icon {
            color: sound_name.color
            name: "stock_music"
            width: units.gu(2)
        }
        Label {
            text: getFileName(timersound)
        }
    }

    ListItem {
        id: edit_sound

        visible: !isRunning
        width: Math.min(parent.width, units.gu(60))
        anchors{
            top: timer_name_field.bottom
            horizontalCenter: parent.horizontalCenter
        }
        divider.visible: false
        SlotsLayout {
            mainSlot: Label {
                id: sound_name
                text: getFileName(timersound)
            }
            Icon {
                color: sound_name.color
                name: "stock_music"
                SlotsLayout.position: SlotsLayout.Leading;
                width: units.gu(2)
            }
            ProgressionSlot {
                color: sound_name.color
            }
        }
        onClicked: {
            sound_ldr.source = Qt.resolvedUrl("SoundSettingExtra.qml")
            sound_ldr.item.temp_sound = timersound
        }
    }

    // TIME SETTER
    TimeSetter {
        id: new_time_setter

        visible: !(root_timesettermain.state == "landscape" && isRunning || circleWidth < time_text.font.pixelSize)
        opacity: showsTime ? 0 : 1
        isInteractive: !isRunning
        anchors {
            top: edit_sound.bottom
            horizontalCenter: parent.horizontalCenter
            verticalCenter: undefined
            verticalCenterOffset: 0
        }
        width: circleWidth
        height: width
        Behavior on scale {NumberAnimation{duration: UbuntuAnimation.BriskDuration}}

    }

    // CENTER CIRCLE
    Rectangle {
        id: smallcentercircle

        visible: new_time_setter.visible
        color: current_theme == "Imitatio" && !showsTime ? "transparent" : main_back_color
        width: showsTime ? 0.7 * circleWidth : 3 * new_time_setter.hour_hand.width / 2
        height: width
        radius: height / 2
        anchors.centerIn: new_time_setter
        Behavior on width {NumberAnimation{duration: UbuntuAnimation.BriskDuration}}

        Label {
            id: extra_time

            opacity: showsTime ? 1 : 0
            color: digit_clock_color
            text: time_text.text
            font.pixelSize: parent.height/6
            anchors.centerIn: parent
            Behavior on opacity {NumberAnimation{duration: UbuntuAnimation.BriskDuration}}
        }

        MouseArea {
            enabled: isRunning
            anchors.fill: parent
            onClicked: showsTime = !showsTime
        }

        // PROGRESS BAR
        ProgressCircle {
            id: progress

            visible: showsTime
            anchors.centerIn: parent

            Connections {
                target: time_text
                onTextChanged: progress.currentValue = (timers_grid.currentItem.remainDuration - 1000) / timers_grid.currentItem.timerDuration * progress.maximumValue
            }
        }
    }

    // START BUTTON
    Button {
        id: start_button

        visible: !isZero // show only when needed
        color: isRunning ? UbuntuColors.red : UbuntuColors.blue
        width: Math.min(parent.width - units.gu(4), units.gu(22) )
        height: units.gu(5)
        anchors{
            bottom: parent.bottom
            bottomMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }

        Icon {
            id: start_button_icon

            color: "#eee"
            name: isRunning ? "media-playback-stop" : "media-playback-start"
            width: parent.height / 2
            anchors.centerIn: parent
        }

        onClicked: {
            isRunning ? stopTimer() : startTimer()
        }
    }

    Rectangle {
        id: timer_finished

        visible: false
        color: main_back_color
        anchors.fill: parent

        Label {
            id: since_finish_label

            width: parent.width - units.gu(4)
            anchors.centerIn: parent

            text: {
                var temptimertime = timeLenghtToDate(timerduration)
                // TRANSLATORS: This string is displayed after a timer has finished.
                // "%1" is replaced by time and the string reads for example:
                // "Timer PIZZA finished 00:02:35 ago".
                // Timer name is not marked for translation
                timername + " (" + temptimertime + ")" + i18n.tr(" finished %1 ago").arg(sinceFinish)
            }
            horizontalAlignment:  Text.AlignHCenter
            wrapMode: Text.WordWrap
            textSize: Label.Large
        }

        Button {
            id: okay_button

            width: Math.min(parent.width - units.gu(4), units.gu(22) )
            height: units.gu(5)
            text: i18n.tr("Okay")
            color: UbuntuColors.blue
            anchors {
                bottom: parent.bottom
                bottomMargin: units.gu(4)
                horizontalCenter: parent.horizontalCenter
            }

            onClicked: {
                timers_grid.currentItem.removeTimer() // delete timer
                timer_preview_ldr.source = ""
                main_page.header = main_header
            }
        }
    }

    Loader {
        id: sound_ldr

        anchors.fill: parent
    }


    states: [
        State {
            name: "landscape"
            when: root_timesettermain.width > root_timesettermain.height && !isHigh
            PropertyChanges {
                target: time_setter
                anchors.horizontalCenterOffset: root_timesettermain.width / 4
                anchors.top: undefined
                anchors.verticalCenter: root_timesettermain.verticalCenter
            }
            PropertyChanges {
                target: start_button;
                anchors.horizontalCenterOffset: isRunning ? 0 : - root_timesettermain.width / 4
            }
            PropertyChanges {
                target: timer_end_time
                width: isRunning ? root_timesettermain.width - units.gu(4) : root_timesettermain.width / 2 - units.gu(4)
                anchors.horizontalCenterOffset: isRunning ? 0 : - root_timesettermain.width / 4
            }

            PropertyChanges {
                target: timer_name_field
                width: root_timesettermain.width / 2 - units.gu(4)
                anchors.horizontalCenterOffset: - root_timesettermain.width / 4
            }

            PropertyChanges {
                target: time_text
                anchors.horizontalCenterOffset: isRunning ? 0 : - root_timesettermain.width / 4
                font.pixelSize: isRunning ? root_timesettermain.width / 5 : root_timesettermain.width / 12
            }

            PropertyChanges {
                target: edit_sound
                width: root_timesettermain.width / 2
                anchors.horizontalCenterOffset: - root_timesettermain.width / 4
            }
        },
        State {
            name: "running"
            when: isRunning
            PropertyChanges {
                target: new_time_setter;
                scale: .9
                anchors.verticalCenter: root_timesettermain.verticalCenter
                anchors.verticalCenterOffset: - units.gu(3)
            }
            PropertyChanges {
                target: time_text;
                font.pixelSize: root_timesettermain.width/5
            }
        }
    ]
}
