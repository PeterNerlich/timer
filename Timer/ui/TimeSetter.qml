import QtQuick 2.4
import Ubuntu.Components 1.3
//import "images"

Item {
    id: root_timesetter

    property int timelength
    property int h: Math.floor(timelength/1000/60/60)
    property int m: Math.floor(timelength/1000/60)
    property int s: Math.floor(timelength/1000)
    property bool isInteractive: true
    property alias hour_hand: timerhandhour

    signal secReleased
    signal minReleased
    signal houReleased

    // Make the mousearea a circle
    function contains(x, y, area) {
        var d = (area.width / 2);
        var dx = (x - area.width / 2);
        var dy = (y - area.height / 2);
        return (d * d > dx * dx + dy * dy);
    }

    height: timerback.height

    /* =================== Analog Timer Background Image ======================*/
    Image {
        id: timerback

        source: Qt.resolvedUrl(/*"images/" + */current_theme + "/secback.svg")
        asynchronous: true
        fillMode: Image.PreserveAspectFit
        sourceSize.width: units.gu(70)
        width: parent.width - units.gu(4)
        anchors.centerIn: parent

        MouseArea {
            id: timerbackmousearea

            property real truex: mouseX-width/2
            property real truey: height/2-mouseY
            property real angle: Math.atan2(truex, truey)
            property real strictangle: Number(angle * 180 / Math.PI)
            property real modulo: strictangle % 6

            enabled: isInteractive
            anchors.fill: parent

            anchors.centerIn: parent
            onPressed: focus = true
            onPositionChanged: {
                if (angle < 0){
                    s = (strictangle - modulo + 360)/6
                    if(s == 60){s=0}
                }
                else {
                    s = (strictangle - modulo + 6)/6
                    if(s == 60){s=0}
                }
            }
            onReleased: root_timesetter.secReleased()
        }

        /* =================== Analog Timer Second Hand ======================*/
        Image {
            id: timerhandsec

            source: Qt.resolvedUrl(/*"images/" +*/ current_theme + "/sechand.svg")
            asynchronous: true
            sourceSize.height: units.gu(70)
            rotation: s * 6
            height: timerback.height
            fillMode: Image.PreserveAspectFit
            anchors.centerIn: parent
        }

        /* =================== Analog Timer Minute Background ======================*/
        Item {
            id: minutesback

            width: timerhandmin.height
            height: width
            anchors.centerIn: parent

            MouseArea {
                id: minmousearea

                property real truex: mouseX-width/2
                property real truey: height/2-mouseY
                property real angle: Math.atan2(truex, truey)
                property real strictangle: Number(angle * 180 / Math.PI)
                property real modulo: strictangle % 6

                enabled: isInteractive
                anchors.fill: parent

                onPressed: {
                    focus = true
                    mouse.accepted = root_timesetter.contains(mouse.x, mouse.y, minmousearea)
                }
                onPositionChanged: {
                    if (angle < 0) {
                        m = (strictangle - modulo + 360)/6
                        if(m == 60){m=0}
                    }
                    else {
                        m = (strictangle - modulo + 6)/6
                        if(m == 60){m=0}
                    }
                }
                onReleased: root_timesetter.minReleased()
            }

            /* =================== Analog Timer Minute Hand ======================*/
            Image {
                id: timerhandmin

                source: Qt.resolvedUrl(/*"images/" + */current_theme + "/minhand.svg")
                sourceSize.height: units.gu(60)
                asynchronous: true
                width: timerhandsec.width
                fillMode: Image.PreserveAspectFit
                rotation: m * 6
                anchors.centerIn: parent
            }

            /* =================== Analog Timer Hour Backgroud ======================*/
            Image {
                id: hourhandbackground

                source: Qt.resolvedUrl(/*"images/" + */current_theme + "/hourback.svg")
                asynchronous: true
                sourceSize.width: units.gu(40)
                width: timerback.width/2
                fillMode: Image.PreserveAspectFit
                anchors.centerIn: parent

                MouseArea {
                    id: hourmousearea

                    enabled: isInteractive
                    property real truex: mouseX-width/2
                    property real truey: height/2-mouseY
                    property real angle: Math.atan2(truex, truey)
                    property real strictangle: Number(angle * 180 / Math.PI)
                    property real modulo: strictangle % 15

                    width: timerhandhour.height
                    height: width
                    anchors.centerIn: parent
                    onPressed: {
                        focus = true
                        mouse.accepted = root_timesetter.contains(mouse.x, mouse.y, hourmousearea)
                    }
                    onPositionChanged:  {
                        if (angle < 0) {
                            h = (strictangle - modulo + 360)/15
                            if(h == 24){h=0}
                        }
                        else {
                            h = (strictangle - modulo + 15)/15
                            if(h == 24){h=0}
                        }
                    }
                    onReleased: root_timesetter.houReleased()
                }

                /* =================== Analog Timer Hour Hand ======================*/
                Image {
                    id: timerhandhour

                    source: Qt.resolvedUrl(/*"images/" + */current_theme + "/hourhand.svg")
                    asynchronous: true
                    sourceSize.height: units.gu(40)
                    width: timerhandsec.width
                    fillMode: Image.PreserveAspectFit
                    rotation: h * 15
                    anchors.centerIn: parent
                }
            }
        }
    }
}
