import QtQuick 2.4
import Ubuntu.Components 1.3
import Qt.labs.settings 1.0
import "Storage.js" as Storage

Item {
    id: root_timesettermain

    property bool isRunning: false
    property bool isZero: time_text.text == "00:00:00"
    property bool timerFinished: false
    property var tempendtime
    property var timerlengthdigits // 00:05:00 or something like this
    property string timername: i18n.tr("Timer")
    property var timersound: defaultsound
    property bool showsTime: false
    property bool isSwitchable: isRunning
    property int timerduration // for progress circle
    property alias time_setter: new_time_setter
    property real restHeight: isUnlocked ? time_text.implicitHeight + timer_end_time.implicitHeight + timer_name_field.implicitHeight + start_button.implicitHeight + edit_sound.implicitHeight + units.gu(5)
                                         : time_text.implicitHeight + timer_end_time.implicitHeight + timer_name_label.implicitHeight + start_button.implicitHeight + units.gu(5)
    property real circleWidth: (root_timesettermain.width > root_timesettermain.height) && !isHigh ? Math.min( root_timesettermain.width / 2, root_timesettermain.height - units.gu(4), units.gu(60) )
                                                                                                   : Math.min( root_timesettermain.width - units.gu(4), root_timesettermain.height - restHeight, ( root_timesettermain.height / 2 - (time_text.implicitHeight + timer_end_time.implicitHeight + timer_name_lbl.implicitHeight + units.gu(1.5)) ) * 2,  units.gu(60))
    property alias timer_label: timer_name_label.text

    function startTimer()
    {
        var endtime = tempEndTime(new_time_setter.h, new_time_setter.m, new_time_setter.s)
        tempendtime = endtime - (endtime % 1000) // round to 1s
        timerduration = new_time_setter.h *60*60*1000 + new_time_setter.m*60*1000 + new_time_setter.s*1000
        timerlengthdigits = timeLenghtToDate(timerduration)
        timername = timer_name_field.displayText
        timername = timername == "" ? i18n.tr("Timer")
                                    : timername
        alarm.reset()
        alarm.date = new Date(tempendtime)
        alarm.message = old_alarm_message = timername + " ("+ timerlengthdigits + ")"
        alarm.sound = getSoundFile(timersound)
        alarm.save()
        isRunning = true
        isTimerRunning = true
        if(timers_model.count >= 1) {
            var stamp = timers_model.get(0).timerStamp
            timers_model.set(0, {
                                 "nameOfTimer": timername,
                                 "endTime": tempendtime,
                                 "soundOfTimer": timersound,
                                 "durationOfTimer": timerduration,
                                 "timerStamp": stamp,
                                 "isItRunning": true
                             }
                             )
            Storage.updateTimers(timername, tempendtime, timerduration, timersound, 1, stamp)
        }
        else {
            var tstamp = Date.now().toString()
            timers_model.append({
                                    "nameOfTimer": timername,
                                    "endTime": tempendtime,
                                    "soundOfTimer": timersound,
                                    "durationOfTimer": timerduration,
                                    "timerStamp": tstamp,
                                    "isItRunning": true
                                })
            Storage.saveTimers(timername, tempendtime, timerduration, timersound, 1, tstamp)
        }

    }

    function stopTimer()
    {
        var currentstamp = timers_model.get(0).timerStamp
        var endtime = timers_model.get(0).endTime
        var timertime = timeLenghtToDate(timers_model.get(0).durationOfTimer)
        var alarm_message = timername + " (" + timertime + ")"
        Storage.deleteTimers(currentstamp)
        isRunning = false
        isTimerRunning = false
        showsTime = false
        alarm_title = ""
        var alarm_date = endtime - (endtime % 1000)
        deleteAlarm(alarm_date, alarm_message)
        lasthvalue = new_time_setter.h
        lastmvalue = new_time_setter.m
        lastsvalue = new_time_setter.s
        timers_model.clear()

    }

    function resetTimer()
    {
        isRunning = isTimerRunning = false
        new_time_setter.h = 0
        new_time_setter.m = 0
        new_time_setter.s = 0
        new_time_setter.timelength = 0
        alarm_title = ""
        lasthvalue = 0
        lastmvalue = 0
        lastsvalue = 0
        if(timers_model.count >= 1) {
            var currentstamp = timers_model.get(0).timerStamp
            Storage.deleteTimers(currentstamp)
            timers_model.clear()
        }

    }

    function remainingTime()
    {
        var start = Date.now()
        var end = new Date(tempendtime)
        var remain = end - start

        if (remain >= 0){
            return [new_time_setter.timelength = remain,

                    timeLenghtToHMS(remain, new_time_setter),
                    timerFinished = false
                    ]
        }

        else {
            return [sinceFinish = timeLenghtToDate(Math.abs(remain)),
                    timerFinished = true,

                    showsTime = false
                    ]
        }
    }

    function endTime()
    {
        var endTimeInMs = tempEndTime(new_time_setter.h, new_time_setter.m, new_time_setter.s)
        var endTimeV = Qt.formatTime(new Date(endTimeInMs), "hh:mm:ss")
        var today = Qt.formatDateTime(new Date(), "d MMM yyyy")
        var endDay = Qt.formatDateTime(new Date(endTimeInMs), "d MMM yyyy")

        if (today === endDay) {
            return endTimeV
        }
        else {
            return i18n.tr("tomorrow") + " " + endTimeV
        }
    }

    Timer {
        id: refreshTimer

        interval: 100
        repeat: true
        running: isRunning

        onTriggered: {
            remainingTime()
        }
    }


    LiveTimer {
        id: checkEndTime

        frequency: isZero ? LiveTimer.Disabled : LiveTimer.Second
        // TRANSLATORS: It's about the time at which a timer will finish,
        // It will read for example "Finish 12:45"
        onTrigger: timer_end_time.text = i18n.tr("Finish") + " " + endTime()
    }

    // RESET TIMER TO ZERO
    ClickyIcon {
        id: reset_button

        visible: !isZero && !isRunning
        iconcolor: UbuntuColors.red
        iconname: "reset"
        anchors {
            right: time_text.left
            rightMargin: units.gu(1)
            verticalCenter: time_text.verticalCenter
        }
        onClicked: resetTimer()
    }



    // TIMER TEXT CLOCK

    Button {
        id: hour_btn

        enabled: !isRunning
        color: "transparent"
        width: time_text.height
        height: width
        anchors {
            left: time_text.left
            verticalCenter: time_text.verticalCenter
        }

        Keys.onPressed: {
            var key = event.key
            if(keys01.indexOf(key) > -1) {
                if(time_setter.h === 0 || time_setter.h >= 2) {
                    time_setter.h = keys01.indexOf(key)
                }
                else {
                    time_setter.h = 10 * time_setter.h + keys01.indexOf(key)
                }
            }
            else if(keys29.indexOf(key) > -1) {
                time_setter.h = keys29.indexOf(key) + 2
            }
            else if(key === Qt.Key_Up) {
                if (time_setter.h < 11) {
                    time_setter.h += 1
                }
            }
            else if(key === Qt.Key_Down) {
                if (time_setter.h > 0) {
                    time_setter.h -= 1
                }
            }
            else if(keyERS.indexOf(key) > -1) {
                if (!isZero) {
                    isRunning ? stopTimer() : startTimer()
                }
            }
        }
    }

    Button {
        id: min_btn

        enabled: !isRunning
        color: "transparent"
        width: time_text.height
        height: width
        anchors.centerIn: time_text

        Keys.onPressed: {
            var key = event.key
            if(keys05.indexOf(key) > -1) {
                if(time_setter.m === 0 || time_setter.m >= 6) {
                    time_setter.m = keys05.indexOf(key)
                }
                else {
                    time_setter.m = 10 * time_setter.m + keys05.indexOf(key)
                }
            }
            else if(keys69.indexOf(key) > -1) {
                if(time_setter.m === 0 || time_setter.m >= 6) {
                    time_setter.m = keys69.indexOf(key) + 6
                }
                else {
                    time_setter.m = 10 * time_setter.m + (keys69.indexOf(key) + 6)
                }
            }
            else if(key === Qt.Key_Up) {
                if (time_setter.m < 59) {
                    time_setter.m += 1
                }
            }
            else if(key === Qt.Key_Down) {
                if (time_setter.m > 0) {
                    time_setter.m -= 1
                }
            }
            else if(keyERS.indexOf(key) > -1) {
                if (!isZero) {
                    isRunning ? stopTimer() : startTimer()
                }
            }
        }
    }

    Button {
        id: sec_btn

        enabled: !isRunning
        color: "transparent"
        width: time_text.height
        height: width
        anchors {
            right: time_text.right
            verticalCenter: time_text.verticalCenter
        }

        Keys.onPressed: {
            var key = event.key
            if(keys05.indexOf(key) > -1) {
                if(time_setter.s === 0 || time_setter.s >= 6) {
                    time_setter.s = keys05.indexOf(key)
                }
                else {
                    time_setter.s = 10 * time_setter.s + keys05.indexOf(key)
                }
            }
            else if(keys69.indexOf(key) > -1) {
                if(time_setter.s === 0 || time_setter.s >= 6) {
                    time_setter.s = keys69.indexOf(key) + 6
                }
                else {
                    time_setter.s = 10 * time_setter.s + (keys69.indexOf(key) + 6)
                }
            }
            else if(key === Qt.Key_Up) {
                if (time_setter.s < 59) {
                    time_setter.s += 1
                }
            }
            else if(key === Qt.Key_Down) {
                if (time_setter.s > 0) {
                    time_setter.s -= 1
                }
            }
            else if(keyERS.indexOf(key) > -1) {
                if (!isZero) {
                    isRunning ? stopTimer() : startTimer()
                }
            }
        }
    }

    // ADD TO FAVS
    ClickyIcon {
        id: add_to_fav_button

        visible: !isZero && !isRunning && !alarm_title
        iconcolor: digit_clock_color
        iconsource: Qt.resolvedUrl("add-to-favs.svg")
        anchors {
            left: time_text.right
            leftMargin: units.gu(1)
            verticalCenter: time_text.verticalCenter
        }
        onClicked: {
            // load saving page
            isOneColumnLayout = false
            apl_main.addPageToNextColumn(apl_main.primaryPage, edit_timer_page, { isEdit: false,isFromMain: true })
            edit_timer_page.time_setter.h = new_time_setter.h
            edit_timer_page.time_setter.m = new_time_setter.m
            edit_timer_page.time_setter.s = new_time_setter.s
            edit_timer_page.timer_name_field.forceActiveFocus()
            edit_timer_page.timer_name_field.text = timername
            alarmsound = timersound
        }
    }

    Text {
        id: time_text

        visible: !showsTime || !smallcentercircle.visible
        color: isRunning ? digit_clock_color : UbuntuColors.blue
        anchors {
            top: parent.top
            horizontalCenter: parent.horizontalCenter
        }
        text: unitsdisplay(new_time_setter.h) + ":" + unitsdisplay(new_time_setter.m) + ":" + unitsdisplay(new_time_setter.s)
        font.pixelSize: isRunning && isLandscape ? parent.width / 3 : Math.min( parent.width / 6, units.gu(10) )
        Behavior on font.pixelSize{UbuntuNumberAnimation{}}
    }

    // TIMER END TIME
    Label {
        id: timer_end_time

        visible: !isZero
        width: parent.width - units.gu(4)
        anchors {
            top: time_text.bottom
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment:  Text.AlignHCenter
    }

    // TIME SETTER
    TimeSetter {
        id: new_time_setter

        visible: !(root_timesettermain.state == "landscape" && isRunning || circleWidth < time_text.font.pixelSize)
        opacity: showsTime ? 0 : 1
        isInteractive: !isRunning
        anchors {
            top: isUnlocked ? edit_sound.bottom : undefined
            horizontalCenter: parent.horizontalCenter
            verticalCenter: isUnlocked ? undefined : parent.verticalCenter
            verticalCenterOffset: isUnlocked ? 0 : - units.gu(3)
        }
        width: circleWidth
        height: width
        onSecReleased: {
            if(timers_model.count === 1) {
                tempendtime = tempEndTime(new_time_setter.h, new_time_setter.m, new_time_setter.s)
                timerduration = new_time_setter.h *60*60*1000 + new_time_setter.m*60*1000 + new_time_setter.s*1000
                var stamp = timers_model.get(0).timerStamp
                timers_model.set(0, {
                                     "nameOfTimer": timername,
                                     "soundOfTimer": timersound,
                                     "durationOfTimer": timerduration,
                                     "timerStamp": stamp,
                                     "isItRunning": false
                                 }
                                 )
                Storage.updateTimers(timername, tempendtime, timerduration, timersound, 0, stamp)
            }
            else {
                lastsvalue = s
                lasttimername = timername
            }
        }
        onMinReleased: {
            if(timers_model.count === 1) {
                tempendtime = tempEndTime(new_time_setter.h, new_time_setter.m, new_time_setter.s)
                timerduration = new_time_setter.h *60*60*1000 + new_time_setter.m*60*1000 + new_time_setter.s*1000
                var stamp = timers_model.get(0).timerStamp
                timers_model.set(0, {
                                     "nameOfTimer": timername,
                                     "soundOfTimer": timersound,
                                     "durationOfTimer": timerduration,
                                     "timerStamp": stamp,
                                     "isItRunning": false
                                 }
                                 )
                Storage.updateTimers(timername, tempendtime, timerduration, timersound, 0, stamp)
            }
            else {
                lastmvalue = m
                lasttimername = timername
            }
        }
        onHouReleased: {
            if(timers_model.count === 1) {
                tempendtime = tempEndTime(new_time_setter.h, new_time_setter.m, new_time_setter.s)
                timerduration = new_time_setter.h *60*60*1000 + new_time_setter.m*60*1000 + new_time_setter.s*1000
                var stamp = timers_model.get(0).timerStamp
                timers_model.set(0, {
                                     "nameOfTimer": timername,
                                     "soundOfTimer": timersound,
                                     "durationOfTimer": timerduration,
                                     "timerStamp": stamp,
                                     "isItRunning": false
                                 }
                                 )
                Storage.updateTimers(timername, tempendtime, timerduration, timersound, 0, stamp)
            }
            else {
                lasthvalue = h
                lasttimername = timername
            }
        }
        Behavior on scale {NumberAnimation{duration: UbuntuAnimation.BriskDuration}}

    }

    // CENTER CIRCLE
    Rectangle {
        id: smallcentercircle

        visible: new_time_setter.visible
        color: current_theme == "Imitatio" && !showsTime ? "transparent" : main_back_color
        width: showsTime ? 0.7 * circleWidth : 3 * new_time_setter.hour_hand.width / 2
        height: width
        radius: height / 2
        anchors.centerIn: new_time_setter
        Behavior on width {NumberAnimation{duration: UbuntuAnimation.BriskDuration}}

        Label {
            id: extra_time

            opacity: showsTime ? 1 : 0
            color: digit_clock_color
            text: time_text.text
            font.pixelSize: parent.height/6
            anchors.centerIn: parent
            Behavior on opacity {NumberAnimation{duration: UbuntuAnimation.BriskDuration}}
        }

        MouseArea {
            enabled: isSwitchable
            anchors.fill: parent
            onClicked: showsTime = !showsTime
        }

        // PROGRESS BAR
        ProgressCircle {
            id: progress

            visible: showsTime
            anchors.centerIn: parent
            Connections {
                target: time_text
                onTextChanged: progress.currentValue = (new_time_setter.timelength - 1000) / timerduration * progress.maximumValue
            }
        }
    }

    // TIMER NAME
    Label {
        id: timer_name_label

        visible: !isUnlocked
        width: parent.width - units.gu(4)
        anchors {
            bottom: start_button.top
            bottomMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        text: timername
        elide: Text.ElideRight
        horizontalAlignment:  Text.AlignHCenter
    }

    // TIMER NAME UNLOCKED
    Label {
        id: timer_name_lbl

        visible: isRunning && isUnlocked
        width: Math.min(parent.width - units.gu(4), units.gu(56))
        anchors {
            top: timer_end_time.bottom
            topMargin: units.gu(.5)
            horizontalCenter: parent.horizontalCenter
        }
        text: timername
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    TextField {
        id: timer_name_field

        visible: !isRunning && isUnlocked
        width: Math.min(parent.width - units.gu(4), units.gu(56))
        anchors {
            top: timer_end_time.bottom
            topMargin: units.gu(1)
            horizontalCenter: parent.horizontalCenter
        }
        text: timername
        placeholderText: i18n.tr("Timer name")
        inputMethodHints: Qt.ImhNoPredictiveText
        onAccepted: {
            timername = text
            if(timers_model.count === 1) {
                var stamp = timers_model.get(0).timerStamp
                timers_model.set(0, {
                                     "nameOfTimer": timername,
                                     "soundOfTimer": timersound,
                                     "durationOfTimer": timerduration,
                                     "timerStamp": stamp,
                                     "isItRunning": false
                                 }
                                 )
                Storage.updateTimers(timername, tempendtime, timerduration, timersound, 0, stamp)
            }
            else {
                lasttimername = timername
            }
        }
    }

    // TIMER SOUND UNLOCKED

    Row {
        id: sound_row

        visible: isRunning && isUnlocked
        spacing: units.gu(1)
        anchors {
            top: timer_name_lbl.bottom
            topMargin: new_time_setter.visible ? 2 * (parent.height / 2 - (time_text.implicitHeight + timer_end_time.implicitHeight + timer_name_lbl.implicitHeight + units.gu(3.5)))
                                               : units.gu(.5)
            horizontalCenter: parent.horizontalCenter
        }
        Icon {
            color: sound_name.color
            name: "stock_music"
            width: units.gu(2)
        }
        Label {
            text: getFileName(timersound)
        }
    }

    ListItem {
        id: edit_sound

        visible: !isRunning && isUnlocked
        width: Math.min(parent.width, units.gu(60))
        anchors{
            top: timer_name_field.bottom
            horizontalCenter: parent.horizontalCenter
        }
        divider.visible: false
        SlotsLayout {
            mainSlot: Label {
                id: sound_name
                text: getFileName(timersound)
            }
            Icon {
                color: sound_name.color
                name: "stock_music"
                SlotsLayout.position: SlotsLayout.Leading;
                width: units.gu(2)
            }
            ProgressionSlot {
                color: sound_name.color
            }
        }
        onClicked: {
            sound_ldr.source = Qt.resolvedUrl("SoundSettingExtra.qml")
            sound_ldr.item.temp_sound = timersound
        }
    }

    // START BUTTON
    Button {
        id: start_button

        visible: !isZero// show only when needed
        color: isRunning ? UbuntuColors.red : UbuntuColors.blue
        width: Math.min(parent.width - units.gu(4), units.gu(22) )
        height: units.gu(6)
        anchors{
            bottom: parent.bottom
            bottomMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }

        Icon {
            id: start_button_icon

            color: "#eee"
            name: isRunning ? "media-playback-stop" : "media-playback-start"
            width: parent.height / 2
            anchors.centerIn: parent
        }

        onClicked: isRunning ? stopTimer() : startTimer()
    }

    Rectangle {
        id: timer_finished

        visible: timerFinished
        color: main_back_color
        anchors.fill: parent

        Label {
            id: since_finish_label

            width: parent.width - units.gu(4)
            anchors.centerIn: parent

            text:  {
                var timerlengthdigits = timeLenghtToDate(timerduration)
                // TRANSLATORS: This string is displayed after a timer has finished.
                // "%1" is replaced by time and the string reads for example:
                // "Timer PIZZA finished 00:02:35 ago".
                // Timer name is not marked for translation
                timername + " (" + timerlengthdigits + ")" + i18n.tr(" finished %1 ago").arg(sinceFinish)
            }
            horizontalAlignment:  Text.AlignHCenter
            wrapMode: Text.WordWrap
            textSize: Label.Large
        }

        Button {
            id: okay_button

            width: Math.min(parent.width - units.gu(4), units.gu(22) )
            height: units.gu(8)
            text: i18n.tr("Okay")
            font.pixelSize: 40
            color: UbuntuColors.blue
            anchors {
                bottom: parent.bottom
                bottomMargin: units.gu(4)
                horizontalCenter: parent.horizontalCenter
            }

            onClicked: {
                resetTimer()
                timerFinished = false
                var alarm_date = tempendtime
                var alarm_message = timername + " (" + timerlengthdigits + ")"
                deleteAlarm(alarm_date, alarm_message)
            }
        }
    }

    Loader {
        id: sound_ldr

        anchors.fill: parent
    }

    states: [
        State {
            name: "landscape"
            when: root_timesettermain.width > root_timesettermain.height && !isHigh
            PropertyChanges {
                target: new_time_setter;
                anchors.horizontalCenterOffset: root_timesettermain.width / 4
                anchors.top: undefined
                anchors.verticalCenter: root_timesettermain.verticalCenter
            }
            PropertyChanges {
                target: time_text;
                anchors.horizontalCenterOffset: isRunning ? 0 : - root_timesettermain.width / 4
                font.pixelSize: isRunning ? root_timesettermain.width / 5 : root_timesettermain.width / 12
            }
            PropertyChanges {
                target: start_button;
                anchors.horizontalCenterOffset: isRunning ? 0 : - root_timesettermain.width / 4
            }
            PropertyChanges {
                target: timer_end_time
                width: isRunning ? root_timesettermain.width - units.gu(4) : root_timesettermain.width / 2 - units.gu(4)
                anchors.horizontalCenterOffset: isRunning ? 0 : - root_timesettermain.width / 4
            }
            PropertyChanges {
                target: timer_name_label
                width: isRunning ? root_timesettermain.width - units.gu(4) : root_timesettermain.width / 2 - units.gu(4)
                anchors.horizontalCenterOffset: isRunning ? 0 : - root_timesettermain.width / 4
            }
            PropertyChanges {
                target: timer_name_field
                width: root_timesettermain.width / 2 - units.gu(4)
                anchors.horizontalCenterOffset: - root_timesettermain.width / 4
            }
            PropertyChanges {
                target: edit_sound
                width: root_timesettermain.width / 2
                anchors.horizontalCenterOffset: - root_timesettermain.width / 4
            }
        },
        State {
            name: "running"
            when: isRunning
            PropertyChanges {
                target: new_time_setter;
                scale: isUnlocked ? .9 : .8
                anchors.verticalCenter: root_timesettermain.verticalCenter
                anchors.verticalCenterOffset: - units.gu(3)
            }
            PropertyChanges {
                target: time_text;
                font.pixelSize: root_timesettermain.width/5
            }
        }
    ]
}
