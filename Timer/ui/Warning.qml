import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
    id: warning_root

    header: PageHeader {
        id: main_header

        title: i18n.tr("Welcome!")
        StyleHints {backgroundColor: top_back_color; foregroundColor: top_text_color}
        leadingActionBar.actions: []
    }

    Flickable {
        anchors {
            top: main_header.bottom
            left: parent.left
            right: parent.right
            bottom: ok_button.top
            bottomMargin: units.gu(1)
        }

        contentHeight: warning_text.height + units.gu(2)


        Label {
            id: warning_text

            anchors {
                top: parent.top
                topMargin: units.gu(2)
                horizontalCenter: parent.horizontalCenter
            }
            width: parent.width - units.gu(4)

            //Warning Label, changes need to go to: /Timer/ui/About.qml, README.md and OpenStore description text
            text: i18n.tr("IMPORTANT!")
                  + "\n\n"
                  + "1. " + i18n.tr("Setting timers shorter than 1 minute is not recommended. Due to system limitations, the notifications about such timers are not displayed at the right time. Sorry.")
                  + "\n\n"
                  + "2. " + i18n.tr("Sound volume is the same as set in the Clock app for alarms.")
                  + "\n\n"
                  + "3. " + i18n.tr("Changing time settings or time zone when timer is running will confuse the timer.")
                  + "\n\n"
                  + "4. " + i18n.tr("Timers are registered as alarms within the clock app. Once the timer has elapsed it is removed from clock app.")
                  + "\n\n"
                  +i18n.tr("Feel free to report other problems.")
            wrapMode: Text.WordWrap
        }
    }

    Button {
        id: ok_button

        color: UbuntuColors.orange
        anchors {
            bottom: parent.bottom
            bottomMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        text: i18n.tr("OK, I understand")
        onClicked: {
            apl_main.removePages(primaryPage)
            warningVisible = false
        }
    }
}
